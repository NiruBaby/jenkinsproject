package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLead extends ProjectMethods{

	public CreateLead() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement eleFName;
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleLName;
	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleComname;
	@FindBy(how = How.CLASS_NAME,using="smallSubmit") WebElement eleClCLead;
	
	public CreateLead enterFName(String Fname) {
		clearAndType(eleFName, Fname);
		return this;
	}
	
public CreateLead enterLName(String Lname) {
		clearAndType(eleLName, Lname);
		return this;
	}
	

public CreateLead enterComName(String Cname) {
	clearAndType(eleComname, Cname);
	return this;
}


public ViewLead clickCreateLead() {
	click(eleClCLead);
	return new ViewLead();
}

}
