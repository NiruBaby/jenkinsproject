package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyLead extends ProjectMethods{

	public MyLead() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
 @FindBy(how = How.XPATH,using="//a[contains(text(). 'Create Lead')]") WebElement eleCreateL;
	public CreateLead clickCreateL() {
		click(eleCreateL);
		return new CreateLead();
	}
}
