package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_Login extends ProjectMethods {
	
	

	@BeforeTest
	public void setData() {
		testCaseName="Login";
		testDescription="Login to leaftaps";
		testNodes="Leads";
		author="kavitha";	
		category="smoke";
		dataSheetName="TC001Login";
		
	}
	
	@Test(dataProvider="fetchData")
	public void login(String Uname,String Pword) {
	new LoginPage() 
		.enterUsername(Uname)
		.enterPassword(Pword)
		.clickLogin();
		

	}
	

	}
	
	

