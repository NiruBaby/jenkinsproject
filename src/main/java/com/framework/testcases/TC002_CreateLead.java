package com.framework.testcases;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC002_CreateLead extends ProjectMethods {
	
	public TC002_CreateLead() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	

	@BeforeTest
	public void setData() {
		testCaseName="TC002_CreateLead";
		testDescription="createLead";
		testNodes="Leads";
		author="kavitha";	
		category="smoke";
		dataSheetName="TC1";
		
	}
	
	@Test(dataProvider="fetchData")
	public void login(String Uname,String Pword,String Fname,String Lname,String Cname) {
	new LoginPage() 
		.enterUsername(Uname)
		.enterPassword(Pword)
		.clickLogin()
		.clickcrmsfa()
		.clickLeads()
		.clickCreateL()
		.enterFName(Fname)
		.enterLName(Lname)
		.enterComName(Cname)
		.clickCreateLead();
	
		

	}
	

	}
	
	


